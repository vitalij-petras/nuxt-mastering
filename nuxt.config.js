export default {
  components: true,
  head: {
    titleTemplate: "Mastering Nuxt: %s",
    htmlAttrs: {
      lang: "en",
    },
    bodyAttrs: {
      class: ["my-style"],
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
    ],
  },
  router: {
    prefetchLinks: false,
  },
  plugins: [
    "~/plugins/dataApi",
    "~/plugins/maps.client",
    "~/plugins/auth.client",
    "~/plugins/vCalendar.client",
    "~/plugins/stripe.client",
  ],
  modules: [
    "~/modules/auth",
    "~/modules/algolia",
    "~/modules/cloudinary",
    "~/modules/stripe",
    "@nuxtjs/cloudinary",
    "@nuxt/image",
    "@nuxtjs/tailwindcss",
  ],
  cloudinary: {
    cloudName: "dqodp4jg9",
  },
  buildModules: [],
  image: {
    cloudinary: {
      baseURL: "https://res.cloudinary.com/dqodp4jg9/image/upload/",
    },
  },
  // serverMiddleware: [
  //   function(req, res, next) {
  //     console.log(req.body);
  //     next();
  //   },
  // ],
  // serverMiddleware: ['serverMiddlewareExamples'],
  css: ["~/assets/sass/app.scss"],
  build: {
    extractCSS: {
      ingnoreOrder: true,
      loaders: {
        limit: 0,
      },
    },
  },
  publicRuntimeConfig: {
    auth: {
      cookieName: "idToken",
      clientId:
        "877098889447-836tcrtktb33tvqsk9iplldqsu6ouaob.apps.googleusercontent.com",
    },
    algolia: {
      appId: "SA3ZFP2M3M",
      key: "043aa30605cca3fe9e00e1f4c3138322",
    },
    cloudinary: {
      apiKey: "341584996245867",
    },
    stripe: {
      key:
        "pk_test_51N24evKul8K3oOUNozFEMsVvt31IQNBAG64Nld4JWaseXPpGWmIdp6JuPhbgDr8gosjkFQ9V4CoHnSrhJXuK2ny000EaJLEydW",
    },
  },
  privateRuntimeConfig: {
    //test2: process.env.TEST
    algolia: {
      appId: "SA3ZFP2M3M",
      key: "132ca51f81640e826602ef8cc07afdec",
    },
    cloudinary: {
      apiSecret: "jd8KkzGD7GGaUedAxop5P_oPdog",
    },
    stripe: {
      secretKey: process.env.STRIPE_SECRET_KEY,
    },
  },
};
