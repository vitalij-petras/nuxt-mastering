// import jwt_decode from "jwt-decode";
// https://developers.google.com/identity/gsi/web/guides/overview
import Cookie from "js-cookie";
import { unWrap } from "~/utils/fetchUtils";

export default ({ $config, store }, inject) => {
  window.onGoogleLibraryLoad = init;
  addScript();

  inject("auth", {
    signOut,
  });

  function addScript() {
    const script = document.createElement("script");
    script.src = "https://accounts.google.com/gsi/client";
    script.async = true;
    document.head.appendChild(script);
  }

  function init() {
    google.accounts.id.initialize({
      client_id: $config.auth.clientId,
      auto_select: true,
      callback: handleResponse,
      // skip_prompt_cookie: $config.auth.cookieName,
    });
    const parent = document.getElementById("googleButton");
    google.accounts.id.renderButton(parent, { theme: "filled_blue" });
    google.accounts.id.prompt();
  }

  async function handleResponse(response) {
    Cookie.set($config.auth.cookieName, response.credential, {
      expires: 1 / 24,
      sameSite: "Lax",
    });

    try {
      const userRresponse = await unWrap(await fetch("/api/user"));
      const user = userRresponse.json;

      store.commit("auth/user", {
        fullName: user.name,
        profileUrl: user.image,
      });
    } catch (error) {
      console.error(error);
    }
  }

  function signOut() {
    google.accounts.id.disableAutoSelect();
    Cookie.remove($config.auth.cookieName);
    store.commit("auth/user", null);
  }
};
