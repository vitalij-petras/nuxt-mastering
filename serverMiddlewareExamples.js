export default (req, res, next) => {
  console.log(req.url);

  if (req.url === "/oldpage") {
    res.statusCode = 302;
    res.setHeader("Location", "/newPage");
    res.end();
    return;
  }

  if (req.url === "/home/c") {
    res.statusCode = 404;
    res.statusMessage = "not found!!!";
    res.end();
    return;
  }

  next();
};
